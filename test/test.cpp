#include "../inc/Matrix.hpp"
#include <stdio.h>

int main() {

	printf("Allocate matrix a\n3 4 5\n2 8 6\n4 0 8\n");
	printf("\n");
	double * array_a = new double[9] {
					3, 4, 5,
					2, 8, 6,
					4, 0, 8};

	Matrix a(array_a, 3, 3);
	delete[] array_a;
	printf("Print matrix a\n");
	a.print();
	printf("\n");

	printf("Allocate matrix b\n5 5 8\n4 2 0\n1 1 9\n");
	printf("\n");
	double * array_b = new double[9] {
					5, 5, 8,
					4, 2, 0, 
					1, 1, 9};	       
	Matrix b(array_b, 3, 3);
	delete[] array_b;
	printf("Print matrix b\n");
	b.print();
	printf("\n");

	printf("Add matrix b to matrix a\n");
	a+=b;
	a.print();
	printf("\n");

	printf("Mulitply matrix a with matrix b\n");
	a*=b;
	a.print();
	printf("\n");

	printf("Allocate matrix c\n5 3 9\n7 1 8\n");
	printf("\n");
	double * array_c = new double[6] {
					5, 3, 9,
					7, 1, 8};
	printf("Print matrix c\n");
	Matrix c(array_c, 2, 3);
	delete[] array_c;
	c.print();
	printf("\n");

	printf("Transpose matrix c\n");
	c.transpose();
	c.print();
	printf("\n");

	printf("Create matrix d = a * c\n");
	printf("\n");
	Matrix d = a * c;
	d.print();
	printf("\n");


	return 0;
}
