#include "Matrix.hpp"
#include <stdio.h>
#include <cstddef>
#include <algorithm>



Matrix::Matrix():
	vdim(0),
	hdim(0),
	array(nullptr) {}

// Copy constructor
Matrix::Matrix(const Matrix& other):
	vdim(other.vdim),
	hdim(other.hdim),
	array(nullptr) {

	// Copy array contents
	std::size_t other_size = other.vdim * other.hdim;
	if(other_size != 0) {
		this->array = new double[other_size];
		std::copy(other.array, other.array + other_size, this->array);
	}
}

// Move constructor
Matrix::Matrix(Matrix&& other):
	Matrix() {
	
	swap(*this, other);
}

Matrix::~Matrix() {
	delete[] this->array;
}

Matrix::Matrix(const std::size_t vdim, const std::size_t hdim):
	vdim(vdim),
	hdim(hdim),
	array(nullptr) {

	std::size_t array_size = this->vdim * this->hdim;
	if(array_size != 0)
		this->array = new double[array_size];
}

// Constuctor for already existing array
// TODO this is a bad idea, there is no way to check of the size is actually right and how this memory was allocated, implemented string parsing thing instead
Matrix::Matrix(const double * array, const std::size_t vdim, const std::size_t hdim):
	vdim(vdim),
	hdim(hdim),
	array(nullptr) {

	// Copy array
	std::size_t array_size = this->vdim * this->hdim;
	if(array_size != 0) {
		this->array = new double[array_size];
		std::copy(array, array + array_size, this->array);
	}
}


void swap(Matrix& first, Matrix& second) {
	using std::swap;

	swap(first.vdim, second.vdim);
	swap(first.hdim, second.hdim);
	swap(first.array, second.array);
}


void Matrix::print() {
	double* matrix_it = this->array;
	for(std::size_t i = 0; i < this->vdim; i++) {
		printf("%5.2f", *matrix_it);
		for(std::size_t j = 1; j < this->hdim; j++) {
			printf(" %5.2f", *(matrix_it+j));
		}
		printf("\n");
		matrix_it += this->hdim;
	}
}

double Matrix::getelem(const unsigned int vindex, const unsigned int hindex) {
	return *(this->array + vindex * this->hdim + hindex);
}

int Matrix::setelem(const unsigned int vindex, const unsigned int hindex, const double value) {
	*(this->array + vindex * this->hdim + hindex) = value;
	return 0;
}


int Matrix::transpose() {
	double * new_array = new double[this->vdim * this->hdim];
	double * new_row_it = new_array;
	double * new_col_it = new_array;

	std::size_t old_vdim = this->vdim;
	std::size_t old_hdim = this->hdim;


	double * array_it = this->array;
	for(std::size_t i = 0; i < old_vdim; i++) {
		for(std::size_t j = 0; j < old_hdim; j++) {
			*new_row_it = *array_it;
			array_it++;
			new_row_it += old_vdim;
		}
		new_col_it++;
		new_row_it = new_col_it;
	}

	delete[] this->array;
	this->array = new_array;
	this->vdim = old_hdim;
	this->hdim = old_vdim;


	return 0;
}


Matrix& Matrix::operator=(Matrix matrix) {
	swap(*this, matrix);
	return *this;

}

Matrix& Matrix::operator+=(const Matrix &matrix) {
	// Check matrix dimensions
	if(this->vdim != matrix.vdim || this->hdim != matrix.hdim) {
		printf("Matrix addition error: Matrix dimensions do not match");
		return *this;
	}

	std::size_t size = this->hdim * this->vdim;
	double* array1_it = this->array;
	double* array2_it = matrix.array;
	for(std::size_t i = 0; i < size; i++) {
		*array1_it += *array2_it;
		array1_it++;
		array2_it++;
	}

	return *this;
}

Matrix& Matrix::operator*=(const Matrix &matrix) {
	// Check matrix dimensions
	if(this->hdim != matrix.vdim) {
		// Error print
		printf("Matrix multiplication error: Horizontal dimension of left matrix is %llu while vertical dimension of right matrix is %llu\n", this->hdim, matrix.vdim);
		return *this;
	}

	// Allocate memory for computed values
	double * new_array = new double[this->vdim * matrix.hdim];

	// Initialize loop variables
	double tmp_sum = 0;
	double * a_col_it = this->array;
	double * a_row_it = this->array;
	double * b_col_it = matrix.array;
	double * b_row_it = matrix.array;
	double * new_array_it = new_array;

	// Iterate over rows of A
	for(std::size_t a_row = 0; a_row < this->vdim; a_row++) {
		// Iterate over columns of B
		for(std::size_t b_col = 0; b_col < matrix.hdim; b_col++) {
			// Do dot product of current a_row and b_col
			for(std::size_t k = 0; k < matrix.vdim; k++) {
				tmp_sum += *a_col_it * *b_row_it;

				// Move A column iterator to next value in row
				a_col_it++;
				// Move B matrix iterator to next row, same column
				b_row_it += matrix.hdim;
			}
			// Set value in new array
			*new_array_it = tmp_sum;
			// Move new array iterator to next value
			new_array_it++;

			// Move A column iterator back to start of row
			a_col_it = a_row_it;
			// Move B column iterator to next column and set row iterator
			b_col_it++;
			b_row_it = b_col_it;
			// Reset temporary sum variable
			tmp_sum = 0;
		}
		// Move A row iterator to next row
		a_row_it += this->hdim;
		a_col_it = a_row_it;
		// Reset B column iterator to start
		b_col_it = matrix.array;
		b_row_it = b_col_it;
	}

	// Update this matrix
	delete[] this->array;
	this->array = new_array;
	this->hdim = matrix.hdim;

	return *this;
}


Matrix operator+(Matrix left, const Matrix& right) {
	left += right;
	return left;
}

Matrix operator*(Matrix left, const Matrix& right) {
	left *= right;
	return left;
}
