#ifndef MATRIX_H
#define MATRIX_H

#include <cstddef>


class Matrix {
	private:
		std::size_t vdim;
		std::size_t hdim;
		double * array;

	public:
		// Constructors
		// Default constructor
		Matrix();
		// Copy constructor
		Matrix(const Matrix& other);
		// Move constructor
		Matrix(Matrix&& other);
		Matrix(const std::size_t vdim, const std::size_t hdim);
		Matrix(const double * array, const std::size_t vdim, const std::size_t hdim);
		~Matrix();

		// Getters, setters
		double getelem(const unsigned int vindex, const unsigned int hindex);
		int setelem(const unsigned int vindex, const unsigned int hindex, const double value);

		// IO
		void print();

		// Matrix operations
		int transpose();

		// Operators
		Matrix& operator+=(const Matrix& matrix);
		Matrix& operator*=(const Matrix& matrix);
		Matrix& operator=(Matrix matrix);

		friend void swap(Matrix& first, Matrix& second);
};

Matrix operator+(Matrix left, const Matrix& right);
Matrix operator*(Matrix left, const Matrix& right);

#endif
