INC = inc
BIN = bin
BUILD = build
LIB = $(BIN)
SRC = src

TARGET = $(LIB)/libmatrix.a
SHARED = $(LIB)/libmatrix.so

WARN = -Wall -Wextra
OPT = -O3 -march=native -s
FLAGS = $(WARN) $(OPT)

CC = g++



# $(TARGET): $(LIB)/$(TARGET)

test: $(BIN)/test
	./$(BIN)/$@



$(BIN)/test: $(BUILD)/test.o $(TARGET)
	$(CC) $< $(FLAGS) -I$(INC) -L$(LIB) -lmatrix -o $@

$(BUILD)/test.o: test/test.cpp
	$(CC) -c $(FLAGS) $? -o $@



$(TARGET): $(BUILD)/matrix.o
	ar rcs $@ $?

$(BUILD)/matrix.o: $(SRC)/matrix.cpp
	$(CC) -c $(FLAGS) -fPIC $? -o $@



.PHONY: clean

clean:
	rm -f $(BUILD)/* $(BIN)/*

.PHONY: dirs

dirs: $(BIN) $(BUILD)

$(BIN):
	mkdir -p $@

$(BUILD):
	mkdir -p $@
